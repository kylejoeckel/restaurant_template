import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Food from '@/components/food/Food'
import Drink from '@/components/drink/Drink'
import Contact from '@/components/contact/Contact'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Main
    },
    {
      path: '/food',
      name: 'Food',
      component: Food
    },
    {
      path: '/drinks',
      name: 'Drinks',
      component: Drink
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    }
  ]
})
